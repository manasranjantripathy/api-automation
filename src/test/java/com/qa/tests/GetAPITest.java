package com.qa.tests;

import java.io.IOException;
import java.util.HashMap;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.client.RestClient;
import com.qa.util.TestUtil;

public class GetAPITest extends TestBase {
	TestBase testBase;
	String serviceUrl;
	String apiUrl;
	String url;
	String konnections_URL;
	String search_URL;
	String KonnectionsSearch_URL;
	RestClient restClient;
	CloseableHttpResponse closableHttpresponse;

	@BeforeMethod
	public void setUp() {
		testBase = new TestBase();
		serviceUrl = prop.getProperty("URL");
		apiUrl = prop.getProperty("serviceURL");
		konnections_URL = prop.getProperty("Konnections_URL");
		search_URL = prop.getProperty("search_URL");

		url = serviceUrl + apiUrl;
		KonnectionsSearch_URL = konnections_URL + search_URL;

	}

	@Test(priority = 1, enabled = false)
	public void getAPITest() throws ClientProtocolException, IOException {
		restClient = new RestClient();
		closableHttpresponse = restClient.get(url);

		// a. Status Code
		int statusCode = closableHttpresponse.getStatusLine().getStatusCode();
		System.out.println("Status Code----" + statusCode);

		Assert.assertEquals(statusCode, RESPONSE_STATUS_CODE_200, "Status code is not 200");

		// b.Json String
		String responseString = EntityUtils.toString(closableHttpresponse.getEntity(), "UTF-8");

		JSONObject responseJson = new JSONObject(responseString);
		System.out.println("Response JSON from APi---" + responseJson);

		String perPageValue = TestUtil.getValueByJPath(responseJson, "/per_page");
		System.out.println("Value of per page is--->" + perPageValue);
		Assert.assertEquals(Integer.parseInt(perPageValue), 3);

		String totalValue = TestUtil.getValueByJPath(responseJson, "/total");
		System.out.println("Value of per page is--->" + totalValue);
		Assert.assertEquals(Integer.parseInt(totalValue), 12);

		String id = TestUtil.getValueByJPath(responseJson, "/data[0]/id");
		String firstName = TestUtil.getValueByJPath(responseJson, "/data[0]/first_name");
		String lastName = TestUtil.getValueByJPath(responseJson, "/data[0]/last_name");
		String avatar = TestUtil.getValueByJPath(responseJson, "/data[0]/avatar");

		System.out.println(id);
		System.out.println(firstName);
		System.out.println(lastName);
		System.out.println(avatar);

		// c.All Headers
		Header[] headersArray = closableHttpresponse.getAllHeaders();

		HashMap<String, String> allHeaders = new HashMap<String, String>();

		for (Header header : headersArray) {
			allHeaders.put(header.getName(), header.getValue());
		}

		System.out.println("Headers Array---" + allHeaders);
	}

	@Test(priority = 2, enabled = false)
	public void getAPITestWithHeaders() throws ClientProtocolException, IOException {
		restClient = new RestClient();

		HashMap<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Content-Type", "application/json");
//		headerMap.put("username", "test@amazon.com");
//		headerMap.put("password", "test123");
//		headerMap.put("Auth Token", "test@amazon.com");

		closableHttpresponse = restClient.get(url, headerMap);

		// a. Status Code
		int statusCode = closableHttpresponse.getStatusLine().getStatusCode();
		System.out.println("Status Code----" + statusCode);

		Assert.assertEquals(statusCode, RESPONSE_STATUS_CODE_200, "Status code is not 200");

		// b.Json String
		String responseString = EntityUtils.toString(closableHttpresponse.getEntity(), "UTF-8");

		JSONObject responseJson = new JSONObject(responseString);
		System.out.println("Response JSON from APi---" + responseJson);

		String perPageValue = TestUtil.getValueByJPath(responseJson, "/per_page");
		System.out.println("Value of per page is--->" + perPageValue);
		Assert.assertEquals(Integer.parseInt(perPageValue), 3);

		String totalValue = TestUtil.getValueByJPath(responseJson, "/total");
		System.out.println("Value of per page is--->" + totalValue);
		Assert.assertEquals(Integer.parseInt(totalValue), 12);

		String id = TestUtil.getValueByJPath(responseJson, "/data[0]/id");
		String firstName = TestUtil.getValueByJPath(responseJson, "/data[0]/first_name");
		String lastName = TestUtil.getValueByJPath(responseJson, "/data[0]/last_name");
		String avatar = TestUtil.getValueByJPath(responseJson, "/data[0]/avatar");

		System.out.println(id);
		System.out.println(firstName);
		System.out.println(lastName);
		System.out.println(avatar);

		// c.All Headers
		Header[] headersArray = closableHttpresponse.getAllHeaders();

		HashMap<String, String> allHeaders = new HashMap<String, String>();

		for (Header header : headersArray) {
			allHeaders.put(header.getName(), header.getValue());
		}

		System.out.println("Headers Array---" + allHeaders);
	}

	@Test(priority = 3, enabled = false)
	public void getAPITestForKonnectionsUserNameAvailable() throws ClientProtocolException, IOException {
		restClient = new RestClient();
		closableHttpresponse = restClient.get(KonnectionsSearch_URL);

		// a. Status Code
		int statusCode = closableHttpresponse.getStatusLine().getStatusCode();
		System.out.println("Status Code----" + statusCode);

		Assert.assertEquals(statusCode, RESPONSE_STATUS_CODE_200, "Status code is not 200");

		// b.Json String
		String responseString = EntityUtils.toString(closableHttpresponse.getEntity(), "UTF-8");

		JSONObject responseJson = new JSONObject(responseString);
		System.out.println("Response JSON from APi---" + responseJson);

		String result = TestUtil.getValueByJPath(responseJson, "/result");
		System.out.println("Value of result is--->" + result);
		Assert.assertEquals(result, "false");

		String errorCode = TestUtil.getValueByJPath(responseJson, "/errorCode");
		System.out.println("Value of errorCode is--->" + errorCode);
		Assert.assertEquals(errorCode, "00");

		// c.All Headers
		Header[] headersArray = closableHttpresponse.getAllHeaders();

		HashMap<String, String> allHeaders = new HashMap<String, String>();

		for (Header header : headersArray) {
			allHeaders.put(header.getName(), header.getValue());
		}

		System.out.println("Headers Array---" + allHeaders);
	}
	

}
