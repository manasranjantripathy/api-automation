package com.qa.tests;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qa.base.TestBase;
import com.qa.client.RestClient;
import com.qa.data.Users;
import com.qa.util.TestUtil;

public class PostAPITest extends TestBase {

	TestBase testBase;
	String serviceUrl;
	String apiUrl;
	String url;
	String konnections_URL;
	String TermsAndCond_URL;
	RestClient restClient;
	CloseableHttpResponse closableHttpresponse;

	@BeforeMethod
	public void setUp() {
		testBase = new TestBase();
		serviceUrl = prop.getProperty("URL");
		apiUrl = prop.getProperty("serviceURL");
		konnections_URL = prop.getProperty("Konnections_URL");
		TermsAndCond_URL = prop.getProperty("TermsAndConditions_URL");
		url = serviceUrl + apiUrl;
		TermsAndCond_URL = konnections_URL + TermsAndCond_URL;

	}

	@Test(enabled = false)
	public void postApiTest() throws JsonGenerationException, JsonMappingException, IOException {
		restClient = new RestClient();
		HashMap<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Content-Type", "application/json");

		// jackson api
		ObjectMapper mapper = new ObjectMapper();
		Users users = new Users("manas", "leader");// expected users object

		// object to jason file (Marshelling)
		mapper.writeValue(new File("C:/Users/Manas/eclipse-workspace/API/src/main/java/com/qa/data/users.json"), users);

		// object to jason in string (Marshelling)
		String userJsonString = mapper.writeValueAsString(users);
		System.out.println(userJsonString);

		closableHttpresponse = restClient.post(url, userJsonString, headerMap);

		// 1.Status code:
		int statusCode = closableHttpresponse.getStatusLine().getStatusCode();
		Assert.assertEquals(statusCode, testBase.RESPONSE_STATUS_CODE_201);

		// 2.JsonString:
		String responseString = EntityUtils.toString(closableHttpresponse.getEntity(), "UTF-8");

		JSONObject responseJson = new JSONObject(responseString);
		System.out.println("The Response from API is:" + responseJson);

		// Json to Java Object (Unmarshelling)
		Users usersResObj = mapper.readValue(responseString, Users.class);// actual users object
		System.out.println(usersResObj);

//		System.out.println(users.getName().equals(usersResObj.getName()));
		Assert.assertTrue(users.getName().equals(usersResObj.getName()));

//		System.out.println(users.getJob().equals(usersResObj.getJob()));
		Assert.assertTrue(users.getJob().equals(usersResObj.getJob()));

	}

	@Test(priority = 2, enabled = true)
	public void getAPITestForKonnectionsVerifyTermsAndConditions() throws ClientProtocolException, IOException {
		restClient = new RestClient();
		HashMap<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Content-Type", "application/x-www-form-urlencoded");
		
		headerMap.put("req", "konnectionsapp@gmail.com");

		// jackson api
		ObjectMapper mapper = new ObjectMapper();
		Users users = new Users("req", "konnectionsapp@gmail.com");// expected users object

		// object to jason file (Marshelling)
		mapper.writeValue(new File("C:/Users/Manas/eclipse-workspace/API/src/main/java/com/qa/data/users.json"), users);

		// object to jason in string (Marshelling)
		String userJsonString = mapper.writeValueAsString(users);
		System.out.println(userJsonString);

		closableHttpresponse = restClient.post(TermsAndCond_URL, userJsonString, headerMap);

		// 1.Status code:
		int statusCode = closableHttpresponse.getStatusLine().getStatusCode();
		Assert.assertEquals(statusCode, testBase.RESPONSE_STATUS_CODE_201);

		// 2.JsonString:
		String responseString = EntityUtils.toString(closableHttpresponse.getEntity(), "UTF-8");

		JSONObject responseJson = new JSONObject(responseString);
		System.out.println("The Response from API is:" + responseJson);

		// Json to Java Object (Unmarshelling)
		Users usersResObj = mapper.readValue(responseString, Users.class);// actual users object
		System.out.println(usersResObj);

//		System.out.println(users.getName().equals(usersResObj.getName()));
		Assert.assertTrue(users.getName().equals(usersResObj.getName()));

//		System.out.println(users.getJob().equals(usersResObj.getJob()));
		Assert.assertTrue(users.getJob().equals(usersResObj.getJob()));
	}

}
